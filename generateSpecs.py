import sys
import os
import argparse
from os import path

class GenerateSpecs:
	def __init__(self):
		self.storage_path = ''
		self.id = 1
		self.labels = []
		self.epochs = 100
		self.batch_size = 4

	def tfrecords(self):
		path = "/workspace/" + str(self.id) + "/training"
		text = "kitti_config {\n\troot_directory_path: \"" + path + "\n\timage_dir_name: \"images\"" + "\n\tlabel_dir_name: \"labels\"\n\t image_extension: \".png\"\n\t partition_mode: \"random\"" + "\n\tnum_partitions: 2\n\t val_split: 20\n\t num_shards: 10\n}\nimage_directory_path: \"" + path + "\""
		if not os.path.exists(os.path.join(self.storage_path, str(self.id) + "/specs")):
			os.makedirs(os.path.join(self.storage_path, str(self.id) + "/specs"))
		f = open(os.path.join(self.storage_path, str(self.id) + "/specs/detectnet_v2_tfrecords_kitti_trainval.txt"), "w+")
		f.write(text)
		f.close()

	def training(self):
		path = "/workspace/" + str(self.id) + "/training"
		# dataset_config
		text = "random_seed: 42\ndataset_config {\n\tdata_sources {\n\t\ttfrecords_path: \"" + path + "/tfrecords/kitti_trainval/*\"\n\t\t" \
		+ "image_directory_path: \"" + path + "\"\n\t}\n\timage_extension: \"png\""
		for lbl in self.labels:
			text = text + "\n\ttarget_class_mapping {\n\t\tkey: \""+lbl+"\"\n\t\tvalue: \""+lbl+"\"\n\t}"
		text = text + "\n\tvalidation_fold: 0\n}"
		# augmentation_config
		text = text + "augmentation_config {\n\tpreprocessing {\n\t\toutput_image_width: 640\n\t\toutput_image_height: 368\n\t\tmin_bbox_width: 1.0\n\t\tmin_bbox_height: 1.0\n\t\toutput_image_channel: 3\n\t"
		text = text + "}\n\tspatial_augmentation {\n\t\thflip_probability: 0.5\n\t\tzoom_min: 1.0\n\t\tzoom_max: 1.0\n\t\ttranslate_max_x: 8.0\n\t\ttranslate_max_y: 8.0\n\t}"
		text = text + "\n\tcolor_augmentation {\n\t\thue_rotation_max: 25.0\n\t\tsaturation_shift_max: 0.2\n\t\tcontrast_scale_max: 0.1\n\t\tcontrast_center: 0.5\n\t}\n}"
		# postprocessing_config
		text = text + "postprocessing_config {"
		for lbl in self.labels:
			text = text + "\n\ttarget_class_config {\n\t\tkey: \""+lbl+"\"\n\t\tvalue {" + "\n\t\t\tclustering_config {\n\t\t\t\tcoverage_threshold: 0.005\n\t\t\t\tdbscan_eps: 0.2\n\t\t\t\tdbscan_min_samples: 0.05\n\t\t\t\tminimum_bounding_box_height: 10\n\t\t\t}\n\t\t}\n\t}"
		text = text + "\n}"
		# model_config
		text = text + "model_config {\n\tpretrained_model_file: \"/workspace/pretrained_resnet18/tlt_pretrained_detectnet_v2_vresnet18/resnet18.hdf5\"\n\tnum_layers: 18\n\tuse_batch_norm: true\n\tobjective_set {" + "\n\t\tbbox {\n\t\t\tscale: 35.0\n\t\t\toffset: 0.5\n\t\t}\n\t\tcov {\n\t\t}\n\t}\n\ttraining_precision {\n\t\tbackend_floatx: FLOAT32\n\t}\n\tarch: \"resnet\"\n}"
		# evaluation_config
		text = text + "evaluation_config {\n\tvalidation_period_during_training: 10\n\tfirst_validation_epoch: 10"
		for lbl in self.labels:
			text = text + "\n\tminimum_detection_ground_truth_overlap {\n\t\tkey: \"" + lbl + "\"\n\t\tvalue: 0.5\n\t}"
		for lbl in self.labels:
			text = text + "\n\tevaluation_box_config {\n\t\tkey: \"" + lbl + "\"\n\t\tvalue {\n\t\t\tminimum_height: 10\n\t\t\tmaximum_height: 9999\n\t\t\tminimum_width: 10\n\t\t\tmaximum_width: 9999\n\t\t}\n\t}"
		text = text + "\n\taverage_precision_mode: INTEGRATE\n}"
		# cost_function_config
		text = text + "cost_function_config {"
		for lbl in self.labels:
			text = text + "\n\ttarget_classes {\n\t\tname: \"" + lbl + "\"\n\t\tclass_weight: 1.0\n\t\tcoverage_foreground_weight: 0.0500000007451\n\t\tobjectives {\n\t\t\tname: \"cov\"\n\t\t\tinitial_weight: 1.0\n\t\t\tweight_target: 1.0\n\t\t}\n\t\tobjectives {\n\t\t\tname: \"bbox\"\n\t\t\tinitial_weight: 10.0\n\t\t\tweight_target: 10.0\n\t\t}\n\t}"
		text = text + "\n\tenable_autoweighting: true\n\tmax_objective_weight: 0.999899983406\n\tmin_objective_weight: 9.99999974739e-05\n}"
		# training_config
		text = text + "training_config {\n\tbatch_size_per_gpu: " + str(self.batch_size) + "\n\tnum_epochs: " + str(self.batch_size) + "\n\tlearning_rate {\n\t\tsoft_start_annealing_schedule {\n\t\t\tmin_learning_rate: 5e-06\n\t\t\tmax_learning_rate: 5e-04\n\t\t\tsoft_start: 0.10000000149\n\t\t\tannealing: 0.699999988079\n\t\t" + "}\n\t}"
		text = text + "\n\tregularizer {\n\t\ttype: L1\n\t\tweight: 3.00000002618e-09\n\t}\n\toptimizer {\n\t\tadam {\n\t\t\tepsilon: 9.99999993923e-09\n\t\t\tbeta1: 0.899999976158\n\t\t\tbeta2: 0.999000012875\n\t\t" + "}\n\t}"
		text = text + "\n\tcost_scaling {\n\t\tinitial_exponent: 20.0\n\t\tincrement: 0.005\n\t\tdecrement: 1.0\n\t}\n\tcheckpoint_interval: 10\n}"
		# bbox_rasterizer_config
		text = text + "bbox_rasterizer_config {"
		for lbl in self.labels:
			text = text + "\n\ttarget_class_config {\n\t\tkey: \"" + lbl + "\"\n\t\tvalue {\n\t\t\tcov_center_x: 0.5\n\t\t\tcov_center_y: 0.5\n\t\t\tcov_radius_x: 0.40000000596\n\t\t\tcov_radius_y: 0.40000000596\n\t\t\tbbox_min_radius: 1.0\n\t\t}\n\t}"
		text = text + "deadzone_radius: 0.400000154972\n}"
		f = open(os.path.join(self.storage_path, str(self.id) + "/specs/detectnet_v2_train_resnet18_kitti.txt"), "w+")
		f.write(text)
		f.close()

	def inference(self):
		# bounding box colors
		if len(self.labels > len(colors) - 1):
			sys.exit("Not enough bounding box colors, add more for inference script generation!")
		colors = ["R: 0\n\t\t\t\tG: 255\n\t\t\t\tB: 0", "R: 255\n\t\t\t\tG: 0\n\t\t\t\tB: 0", "R: 0\n\t\t\t\tG: 0\n\t\t\t\tB: 255", "R: 255\n\t\t\t\tG: 255\n\t\t\t\tB: 0", "R: 0\n\t\t\t\tG: 255\n\t\t\t\tB: 255", "R: 255\n\t\t\t\tG: 0\n\t\t\t\tB: 255", "R: 0\n\t\t\t\tG: 0\n\t\t\t\tB: 0", "R: 255\n\t\t\t\tG: 255\n\t\t\t\tB: 255", "R: 100\n\t\t\t\tG: 0\n\t\t\t\tB: 100", "R: 0\n\t\t\t\tG: 100\n\t\t\t\tB: 100"]
		# inferencer_config
		text = "inferencer_config{"
		for lbl in self.labels:
			text = text + "\n\ttarget_classes: \"" + lbl + "\""
		text = text + "\n\timage_width: 640\n\timage_height: 368\n\timage_channels: 3\n\tbatch_size: 16\n\tgpu_index: 0\n\ttlt_config{\n\t\tmodel: \"/workspace/" + str(self.id) + "/experiment_dir_unpruned/weights/resnet18_detector.tlt\n\t}\n}"
		# bbox_handler_config
		count = 0
		text = text + "bbox_handler_config{\n\tkitti_dump: true\n\tdisable_overlay: false\n\toverlay_linewidth: 2"
		for lbl in self.labels:
			text = text + "\n\tclasswise_bbox_handler_config{\n\t\tkey: \"" + lbl + "\"\n\t\tvalue: {\n\t\t\tconfidence_model: \"aggregate_cov\"\n\t\t\toutput_map: \"" + lbl + "\"\n\t\t\tconfidence_threshold: 0.9\n\t\t\tbbox_color{\n\t\t\t\t" + colors[count] + "\n\t\t\t}\n\t\t\tclustering_config{\n\t\t\t\tcoverage_threshold: 0.00\n\t\t\t\tdbscan_eps: 0.3\n\t\t\t\tdbscan_min_samples: 0.05\n\t\t\t\tminimum_bounding_box_height: 4\n\t\t\t}\n\t\t}\n\t}"
			count = count + 1
		text = text + "\n\tclasswise_bbox_handler_config{\n\t\tkey: \"default\"\n\t\tvalue: {\n\t\t\tconfidence_model: \"aggregate_cov\"\n\t\t\tconfidence_threshold: 0.01\n\t\t\tbbox_color{\n\t\t\t\t" + colors[count] + "\n\t\t\t}\n\t\t\tclustering_config{\n\t\t\t\tcoverage_threshold: 0.00\n\t\t\t\tdbscan_eps: 0.3\n\t\t\t\tdbscan_min_samples: 0.05\n\t\t\t\tminimum_bounding_box_height: 4\n\t\t\t}\n\t\t}\n\t}"
		text = text + "\n}"
		f = open(os.path.join(self.storage_path, str(self.id) + "/specs/detectnet_v2_inference_kitti_tlt.txt"), "w+")
		f.write(text)
		f.close()


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Running specs generation scripts')
	parser.add_argument('storage_path', type=str, help='Location of local storage')
	parser.add_argument('sess_id', type=int, help='Session ID')
	parser.add_argument('labels', type=str, help='Labels for objects being trained')
	parser.add_argument('batch_size', type=int, help='Batch size per gpu for training')
	parser.add_argument('epochs', type=int, help='Number of Epochs to run training')
	args = parser.parse_args()
	if not path.exists(args.storage_path):
		sys.exit("Invalid storage path given!")
	if not path.exists(args.storage_path + "/" + str(args.sess_id)):
		sys.exit("Invalid session ID given!")
	sess = GenerateSpecs()
	sess.storage_path = args.storage_path
	sess.id = args.sess_id
	sess.labels = args.labels.split(',')
	sess.batch_size = args.batch_size
	sess.epochs = args.epochs
	sess.tfrecords()
	sess.training()
	sess.inference()
	exit(200)

  