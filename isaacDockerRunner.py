import sys
import docker
import os
import argparse
from os import path


class RunIsaac:
	def __init__(self):
		self.storage_path = ''
		self.id = 1
		self.container_id = ''
		self.key = '1234'
		self.base_path = ''
		self.mount_path = ''

	def start_container(self):
		os.system(
			'docker run --gpus all -d -it -v ' + self.base_path + ':' + self.mount_path + ' -p \
			8888:8888 nvcr.io/nvidia/tlt-streamanalytics:v2.0_dp_py2')
		client = docker.from_env()
		for container in client.containers.list():
			self.container_id = container.id

	def generate_tf_records(self):
		os.system("docker exec -it " + self.container_id + " tlt-dataset-convert -d " + self.mount_path +
				  "specs/detectnet_v2_tfrecords_kitti_trainval.txt -o " + self.mount_path +
				  "training/tfrecords/kitti_trainval/kitti_trainval")
		os.system("docker stop " + self.container_id)

	def start_training(self):
		os.system(
			"docker exec -it " + self.container_id + " tlt-train detectnet_v2 -e " + self.mount_path +
			"specs/detectnet_v2_train_resnet18_kitti.txt -r " + self.mount_path +
			"experiment_dir_unpruned -k " + self.key + " -n resnet18_detector")
		os.system("docker stop " + self.container_id)

	def run_inference(self):
		os.system(
			"docker exec -it " + self.container_id + " tlt-infer detectnet_v2 -e " + self.mount_path +
			"specs/detectnet_v2_inference_kitti_tlt.txt -o " + self.mount_path + "tlt_infer_testing \
			-i /workspace/tlt-experiments/training_data/testing/image_2 -k " + self.key)
		os.system("docker stop " + self.container_id)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Running docker scripts')
	parser.add_argument('storage_path', type=str, help='Location of local storage')
	parser.add_argument('sess_id', type=int, help='Session ID')
	parser.add_argument('action', type=int, choices=[1, 2, 3], help='1:tfrecords, 2:train, 3:inference')
	args = parser.parse_args()
	if not path.exists(args.storage_path):
		sys.exit("Invalid storage path given!")
	if not path.exists(args.storage_path + "/" + str(args.sess_id)):
		sys.exit("Invalid session ID given!")
	sess = RunIsaac()
	sess.storage_path = args.storage_path
	sess.id = args.sess_id
	sess.base_path = sess.storage_path + str(sess.id) + "/"
	sess.mount_path = "/workspace/" + str(sess.id) + "/"
	if args.action == 1:
		sess.start_container()
		sess.generate_tf_records()
	elif args.action == 2:
		sess.start_container()
		sess.start_training()
	else:
		sess.start_container()
		sess.run_inference()
	exit(200)
