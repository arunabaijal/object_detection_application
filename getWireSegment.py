import numpy as np
import argparse
from os import path, walk
import os
import sys
import cv2
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

def ColorDistance(rgb1,rgb2):
	rm = 0.5*(rgb1[0]+rgb2[0])
	d = sum((2+rm,4,3-rm)*(rgb1-rgb2)**2)**0.5
	return d

class WireSegmentation:
	def __init__(self):
		self.storage_path = ''
		self.id = 1
		self.frame_path = ''
		self.frames = []
		self.frame_names = []
		self.wire_names = []
		self.wire_colors = []
		self.wire_contours = []

	def generateFramePath(self):
		num = len(next(os.walk(os.path.join(self.storage_path, str(self.id) + "/testing")))[1])
		self.frame_path = os.path.join(self.storage_path, str(self.id) + "/testing/" + str(num))

	def getFrames(self):
		# frames.rstrip("\n")
		# frame_names = frames.split(",")
		
		# for name in frame_names:
		for (dirpath, dirnames, filenames) in walk(self.frame_path + "/images/"):
			for filename in filenames:
				print(filename)
				self.frames.append(cv2.imread(self.frame_path + "/images/" + filename))
				self.frame_names.append(filename)

	def getWireDetails(self, wire_name, wire_center):
		print(wire_name)
		print(wire_center)
		wire_name.rstrip("\n")
		wire_center.rstrip("\n")
		names = wire_name.split(",")
		colors = wire_center.split(":")
		if len(names) != len(colors):
			sys.exit("Wire labels not given correctly!")
		for i in range(len(names)):
			self.wire_names.append(names[i])
			self.wire_colors.append(colors[i])

	def createJson(self):
		json = '{'
		for num in range(len(self.frames)):
			if len(self.wire_contours[num]) != len(self.wire_names):
				print("Error in image, skipping frame!")
				continue
			json = json + "\"" + self.frame_names[num] + "\":{"
			for wire_num in range(len(self.wire_names)):
				json = json + "\"" + self.wire_names[wire_num] + "\":\""
				trim = False
				for c in self.wire_contours[num][wire_num]:
					json = json + "[" + str(c) + "],"
					trim = True
				if trim:
					json = json[:-1]
				json = json + "\","
			json = json[:-1]
			json = json + "},"
		json = json[:-1]
		json = json + "}"
		return json

	def findWires(self):
		image = self.frames[0]
		pic = cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
		pic_n = pic.reshape(pic.shape[0] * pic.shape[1], pic.shape[2])
		cluster_size = 16
		kmeans = KMeans(n_clusters=cluster_size, random_state=0).fit(pic_n)
		pic2show = kmeans.cluster_centers_[kmeans.labels_]
		wire_center_xy = self.wire_colors[0].split(",")
		cluster_pic = pic2show.reshape(pic.shape[0], pic.shape[1], pic.shape[2])
		wire_center = cluster_pic[int(float(wire_center_xy[0][1:]))][int(float(wire_center_xy[1][1:-1]))]
		print(wire_center)

		print(len(self.frames))
		for image in self.frames:
			wire_points = []
			for wire_num in range(len(self.wire_names)):
				pic = cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
				pic_n = pic.reshape(pic.shape[0] * pic.shape[1], pic.shape[2])
				cluster_size = 16
				kmeans = KMeans(n_clusters=cluster_size, random_state=0).fit(pic_n)
				pic2show = kmeans.cluster_centers_[kmeans.labels_]
				# cluster_pic = pic2show.reshape(pic.shape[0], pic.shape[1], pic.shape[2])
				# plt.imshow(cluster_pic, cmap='Greys')
				# plt.show()
				# wire_center_rgb = self.wire_colors[wire_num].split(",")
				# wire_center = [float(wire_center_rgb[0][1:]), float(wire_center_rgb[1]), float(wire_center_rgb[2][:-1])]
				min_dist = sys.maxsize
				min_ind = 0
				for ind, cluster in enumerate(kmeans.cluster_centers_):
					distance = ColorDistance(cluster, wire_center)
					if distance < min_dist:
						min_dist = distance
						min_ind = ind

				for ind, pixel in enumerate(kmeans.labels_):
					if not np.array_equal(pixel, min_ind):
						pic2show[ind] = np.zeros(3)
					else:
						pic2show[ind] = np.ones(3)
				cluster_pic = pic2show.reshape(pic.shape[0], pic.shape[1], pic.shape[2])
				img_grayscale = cv2.cvtColor(cluster_pic, cv2.COLOR_BGR2GRAY)
				img_grayscale = img_grayscale.astype('uint8')
				kernel = np.ones((5, 5), np.uint8)
				dilation = cv2.dilate(img_grayscale, kernel, iterations=2)
				contours, hierarchy = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
				threshold_area = 10000
				relContours = []
				for contour in contours:
					area = cv2.contourArea(contour)
					# print(area)
					if area > threshold_area:
						relContours.append(contour)

				empty_image = np.zeros(image.shape)
				cv2.drawContours(empty_image, relContours, -1, (255, 255, 255), -1)
				empty_image = empty_image.astype('uint8')
				empty_image = cv2.cvtColor(empty_image, cv2.COLOR_BGR2GRAY)
				# plt.imshow(empty_image, cmap='Greys')
				# plt.show()
				dilation_2 = cv2.dilate(empty_image, kernel, iterations=2)
				# plt.imshow(dilation_2, cmap='Greys')
				# plt.show()
				# closing = cv2.morphologyEx(empty_image, cv2.MORPH_CLOSE, kernel_2)
				contours, hierarchy = cv2.findContours(dilation_2, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
				empty_image = np.zeros(image.shape)
				cv2.drawContours(empty_image, contours, -1, (255, 255, 255), -1)
				empty_image = empty_image.astype('uint8')
				empty_image = cv2.cvtColor(empty_image, cv2.COLOR_BGR2GRAY)
				erosion = cv2.erode(empty_image, kernel, iterations=2)
				contours, hierarchy = cv2.findContours(erosion, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
				sin_wire_points = []
				for c in contours:
					for pts in c:
						sin_wire_points.append((pts[0][0], pts[0][1]))
				wire_points.append(sin_wire_points)
			self.wire_contours.append(wire_points)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Run wire segmentation on testing video')
	parser.add_argument('storage_path', type=str, help='Location of local storage')
	parser.add_argument('sess_id', type=int, help='Session ID')
	# parser.add_argument('frames', type=str, help='List of frames with wire')  # comma separated frame names
	parser.add_argument('wire_name', type=str, help='Label name for wire')  # comma separated wire names
	parser.add_argument('wire_center', type=str, help='Point to read wire color from')  # colon separated wire center
	args = parser.parse_args()
	if not path.exists(args.storage_path):
		sys.exit("Invalid storage path given!")
	if not path.exists(args.storage_path + "/" + str(args.sess_id)):
		sys.exit("Invalid session ID given!")
	sess = WireSegmentation()
	sess.storage_path = args.storage_path
	sess.id = args.sess_id
	sess.generateFramePath()
	sess.getFrames()
	sess.getWireDetails(args.wire_name, args.wire_center)
	sess.findWires()
	json = sess.createJson()
	if not os.path.exists(sess.frame_path + "/labels/wire/"):
		os.makedirs(sess.frame_path + "/labels/wire/")
	f = open(sess.frame_path + "/labels/wire/wire_contours.txt", "w+")
	f.write(json)
	f.close()
	exit(200)


# demo run: /home/siminsights/object_detection_application/storage 2 20201113_145955.jpg,20201113_145959.jpg,20201113_150003.jpg,20201113_150012.jpg red (0.141, 0.2, 0.343)