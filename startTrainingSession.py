import argparse
from os import path
import os
import sys
import cv2


class TrainingSession:
	def __init__(self):
		self.storage_path = ''
		self.video_path = ''
		self.id = 1

	def generate_session_id(self):
		self.id = len(next(os.walk(self.storage_path))[1])

	def extract_frames(self):
		# width to resize
		width = 640
		# height to resize
		height = 368
		img_path = os.path.join(self.storage_path, str(self.id) + "/training/images")
		print("Frame Output Path: " + img_path)

		if not os.path.exists(img_path):
			os.makedirs(img_path)

		if len(os.listdir(img_path)) != 0:
			print("Files already in target frame directory, skipping frame extract")
			return

		vidcap = cv2.VideoCapture(self.video_path)
		images = []
		success,image = vidcap.read()
		while success:
			images.append(image)
			success,image = vidcap.read()

		# get all the pictures in directory
		count = 0
		for img in images:
			h, w = img.shape[:2]
			ratio = w / h

			if h > height or w > width:
				# shrinking image algorithm
				interp = cv2.INTER_AREA
			else:
				# stretching image algorithm
				interp = cv2.INTER_CUBIC

			w = width
			h = round(w / ratio)
			if h > height:
				h = height
				w = round(h * ratio)
			pad_bottom = abs(height - h)
			pad_right = abs(width - w)

			scaled_img = cv2.resize(img, (w, h), interpolation=interp)
			padded_img = cv2.copyMakeBorder(
				scaled_img, 0, pad_bottom, 0, pad_right, borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0])

			cv2.imwrite(str(img_path) + "/frame%04d.png" % count, padded_img)
			count = count + 1


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Process training video')
	parser.add_argument('storage_path', type=str, help='Location of local storage')
	parser.add_argument('video', type=str, help='Location of training video')
	args = parser.parse_args()
	if not path.exists(args.storage_path):
		sys.exit("Invalid storage path given!")
	if not path.exists(args.video):
		sys.exit("Invalid video path given!")
	sess = TrainingSession()
	sess.storage_path = args.storage_path
	sess.video_path = args.video
	sess.generate_session_id()
	sess.extract_frames()
	exit(200)
