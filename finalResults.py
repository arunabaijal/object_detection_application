import json
import argparse
import os
from os import path, walk
import sys


def comp(list1, list2):
	for val in list1:
		if val in list2:
			return True
	return False

def get_line(x1, y1, x2, y2):
	points = []
	issteep = abs(y2-y1) > abs(x2-x1)
	if issteep:
		x1, y1 = y1, x1
		x2, y2 = y2, x2
	rev = False
	if x1 > x2:
		x1, x2 = x2, x1
		y1, y2 = y2, y1
		rev = True
	deltax = x2 - x1
	deltay = abs(y2-y1)
	error = int(deltax / 2)
	y = y1
	ystep = None
	if y1 < y2:
		ystep = 1
	else:
		ystep = -1
	for x in range(x1, x2 + 1):
		if issteep:
			points.append((y, x))
		else:
			points.append((x, y))
		error -= deltay
		if error < 0:
			y += ystep
			error += deltax
	# Reverse the list if the coordinates were reversed
	if rev:
		points.reverse()
	return points


class CombineInferences:
	def __init__(self):
		self.storage_path = ''
		self.id = 1
		self.test_path = ''
		self.objects = set()
		self.wires = set()
		self.wire_connections = set()

	def generate_test_path(self, test_iter):
		self.test_path = os.path.join(self.storage_path, str(self.id) + "/testing/" + str(test_iter))

	def get_objects(self):
		if not path.exists(self.test_path + "/labels"):
			sys.exit("No inference results found!")
		if not path.exists(self.test_path + "/labels/tlt/labels"):
			print("No neural network inference on objects performed!")
		else:
			ext = (".txt")
			for (_, _, filenames) in walk(self.test_path + "/labels/tlt/labels"):
				for filename in filenames:
					if filename.endswith(ext):
						f = open(os.path.join(self.test_path + "/labels/tlt/labels", filename), "r")
						for line in f:
							line = line.rstrip("\n")
							li = line.split(' ')
							self.objects.add(li[0])
						f.close()
		if not path.exists(self.test_path + "/labels/wire"):
			print("No wires!")
		else:
			ext = (".txt")
			for (_, _, filenames) in walk(self.test_path + "/labels/wire"):
				for filename in filenames:
					if filename.endswith(ext):
						with open(os.path.join(self.test_path + "/labels/wire", filename), 'r') as file:
							data = json.load(file)
						keys = data.keys()
						for k in keys:
							[self.wires.add(sub_key) for sub_key in data[k].keys()]
		# print(self.objects)
		# print(self.wires)

	def get_wire_connections(self):
		ext = (".txt")
		for (_, _, filenames) in walk(self.test_path + "/labels/wire"):
			for filename in filenames:
				if filename.endswith(ext):
					with open(os.path.join(self.test_path + "/labels/wire", filename), 'r') as file:
						data = json.load(file)
					keys = data.keys()
					for frame in keys:
						# [self.wires.add(sub_key) for sub_key in data[k].keys()]
						for wire_color in data[frame].keys():
							if not path.exists(self.test_path + "/labels/tlt/labels/" + frame):
								print("Neural network detection not found for frame", frame)
								continue
							f = open(self.test_path + "/labels/tlt/labels/" + frame, "r")
							for line in f:
								line = line.rstrip("\n")
								# print("line", line)
								li = line.split(' ')
								x1 = int(float(li[4]))
								y1 = int(float(li[5]))
								x2 = int(float(li[6]))
								y2 = int(float(li[7]))
								contour_center_x = 0.5 * (float(li[4]) + float(li[6]))
								contour_center_y = 0.5 * (float(li[5]) + float(li[7]))
								if comp(get_line(x1, y1, x1, y2), data[frame][wire_color]):
									print("Wire intersects with the object " + li[0])
									self.wire_connections.add(wire_color + "connects" + li[0])
								elif comp(get_line(x1, y1, x2, y1), data[frame][wire_color]):
									print("Wire intersects with the object " + li[0])
									self.wire_connections.add(wire_color + "connects" + li[0])
								elif comp(get_line(x2, y1, x2, y2), data[frame][wire_color]):
									print("Wire intersects with the object " + li[0])
									self.wire_connections.add(wire_color + "connects" + li[0])
								elif comp(get_line(x1, y2, x2, y2), data[frame][wire_color]):
									print("Wire intersects with the object " + li[0])
									self.wire_connections.add(wire_color + "connects" + li[0])
								# print(contour_center_x, contour_center_y)
								# print(cX, cY)
								# if cX > contour_center_x:
								# 	print("Wire is to the right of the object " + li[0])
								# elif cX < contour_center_x:
								# 	print("Wire is to the right of the object " + li[0])
								# if cY > contour_center_y:
								# 	print("Wire is below the object " + li[0])
								# elif cY < contour_center_y:
								# 	print("Wire is above the object " + li[0])



if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Run inference on testing video')
	parser.add_argument('storage_path', type=str, help='Location of local storage')
	parser.add_argument('sess_id', type=int, help='Session ID')
	parser.add_argument('test_iter', type=int, help='Test iteration number')
	args = parser.parse_args()
	if not path.exists(args.storage_path):
		sys.exit("Invalid storage path given!")
	if not path.exists(args.storage_path + "/" + str(args.sess_id)):
		sys.exit("Invalid session ID given!")
	sess = CombineInferences()
	sess.storage_path = args.storage_path
	sess.id = args.sess_id
	sess.generate_test_path(args.test_iter)
	if not path.exists(sess.test_path):
		sys.exit("Invalid test iteration given!")
	sess.get_objects()
	print(sess.objects)
	print(sess.wires)
	if path.exists(sess.test_path + "/labels/wire"):
		sess.get_wire_connections()
		print(sess.wire_connections)
	exit(200)
