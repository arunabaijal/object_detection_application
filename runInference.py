import argparse
from os import path
import os
import sys
import cv2


class TestingSession:
	def __init__(self):
		self.storage_path = ''
		self.video_path = ''
		self.id = 1
		self.test_path = ''
		self.angle = 4

	def generate_test_path(self):
		if not os.path.exists(os.path.join(self.storage_path, str(self.id) + "/testing")):
			os.makedirs(os.path.join(self.storage_path, str(self.id) + "/testing"))
		num = len(next(os.walk(os.path.join(self.storage_path, str(self.id) + "/testing")))[1]) + 1
		self.test_path = os.path.join(self.storage_path, str(self.id) + "/testing/" + str(num) + "/labels/tlt")
		os.makedirs(self.test_path)

	def extract_frames(self):
		# width to resize
		width = 640
		# height to resize
		height = 368
		img_path = self.test_path + "/images"
		print("Frame Output Path: " + img_path)

		if not os.path.exists(img_path):
			os.makedirs(img_path)

		if len(os.listdir(img_path)) != 0:
			print("Files already in target frame directory, skipping frame extract")
			return

		vidcap = cv2.VideoCapture(self.video_path)
		images = []
		success, image = vidcap.read()
		while success:
			if self.angle == 1:
				image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
			elif self.angle == 2:
				image = cv2.rotate(image, cv2.ROTATE_180)
			elif self.angle == 3:
				image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
			images.append(image)
			success, image = vidcap.read()

		# get all the pictures in directory
		count = 0
		for img in images:
			h, w = img.shape[:2]
			ratio = w / h

			if h > height or w > width:
				# shrinking image algorithm
				interp = cv2.INTER_AREA
			else:
				# stretching image algorithm
				interp = cv2.INTER_CUBIC

			w = width
			h = round(w / ratio)
			if h > height:
				h = height
				w = round(h * ratio)
			pad_bottom = abs(height - h)
			pad_right = abs(width - w)

			scaled_img = cv2.resize(img, (w, h), interpolation=interp)
			padded_img = cv2.copyMakeBorder(
				scaled_img, 0, pad_bottom, 0, pad_right, borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0])

			cv2.imwrite(str(img_path) + "/frame%04d.png" % count, padded_img)
			count = count + 1

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Run inference on testing video')
	parser.add_argument('storage_path', type=str, help='Location of local storage')
	parser.add_argument('sess_id', type=int, help='Session ID')
	parser.add_argument('video', type=str, help='Location of testing video')
	parser.add_argument('rotation', type=int, help='Rotate 1. 90 deg clockwise 2. flip 180 deg 3. 90 deg anticlockwise 4. remain same')
	args = parser.parse_args()
	if not path.exists(args.storage_path):
		sys.exit("Invalid storage path given!")
	if not path.exists(args.storage_path + "/" + str(args.sess_id)):
		sys.exit("Invalid session ID given!")
	if not path.exists(args.video):
		sys.exit("Invalid video path given!")
	sess = TestingSession()
	sess.storage_path = args.storage_path
	sess.video_path = args.video
	sess.id = args.sess_id
	sess.angle = args.rotation
	sess.generate_test_path()
	sess.extract_frames()
	exit(200)
